# pv-display-helper

Supporting library for implementing the PV display protocol for vGlass.

# Build instructions

To build and install the module, it requires the "kernel" headers for IVC. IVC's build system will, by default, install them to `/usr/src/ivc/include`. Setting `IVC_INCLUDE_DIR` will override this path.
```
make IVC_INCLUDE_DIR=/usr/src/ivc/include
make modules_install
```

`pv-display-helper` exports public headers for userspace and kernel use. These are installed by default in `/usr/src/pv_display_helper/include`. Setting  `INSTALL_HDR_PATH` will override this path.
```
make INSTALL_HDR_PATH=/usr/src/pv_display_helper headers_install
```
