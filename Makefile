ifneq ($(KERNELRELEASE),)
# kbuild part of makefile
include Kbuild

else
# normal makefile
KERNEL_VERSION ?= `uname -r`
KERNEL_SRC ?= /lib/modules/$(KERNEL_VERSION)/build
INSTALL_HDR_PATH ?= /usr/src/pv_display_handler

IVC_INCLUDE_DIR ?= /usr/src/ivc/include

default:
	$(MAKE) -C $(KERNEL_SRC) M=$$PWD

# Conflict with userspace.
#clean:
#	$(MAKE) -C $(KERNEL_SRC) M=$$PWD clean

modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$$PWD modules_install

headers_install:
	install -d $(INSTALL_HDR_PATH)/include
	install -m 0644 common.h $(INSTALL_HDR_PATH)/include/common.h
	install -m 0644 pv_driver_interface.h $(INSTALL_HDR_PATH)/include/pv_driver_interface.h
	install -m 0644 pv_display_helper.h $(INSTALL_HDR_PATH)/include/pv_display_helper.h


DESTDIR ?= ""

libraries: libpvdisplayhelper.so libpvbackendhelper.so

libpvdisplayhelper.so: pv_display_helper.c
	$(CC) $(CFLAGS) -I. -W -Wall -Werror -fPIC $(LDFLAGS) -shared -fPIC -o $@ $^

libpvbackendhelper.so: pv_display_backend_helper.c
	$(CC) $(CFLAGS) -I. -W -Wall -Werror -fPIC $(LDFLAGS) -shared -fPIC -o $@ $^

libraries_install: libraries
	install -D -m 644 pv_display_helper.h "$(DESTDIR)$(includedir)/pv_display_helper.h"
	install -D -m 644 pv_driver_interface.h "$(DESTDIR)$(includedir)/pv_driver_interface.h"
	install -D -m 644 data-structs/list.h "$(DESTDIR)$(includedir)/data-structs/list.h"
	install -D -m 644 common.h "$(DESTDIR)$(includedir)/common.h"
	install -D -m 755 libpvdisplayhelper.so "$(DESTDIR)$(libdir)/libpvdisplayhelper.so"
	install -D -m 644 pv_display_backend_helper.h "$(DESTDIR)$(includedir)/pv_display_backend_helper.h"
	install -D -m 755 libpvbackendhelper.so "$(DESTDIR)$(libdir)/libpvbackendhelper.so"

clean:
	rm -f $(wildcard *.o) $(wildcard *.ko) $(wildcard *.so)

endif
